const express = require('express');
const path = require('path');

const app = express();
app.use(express.static("./client/build"));

app.get('/*', (req, res) => {			// Serving the react build from express
    res.sendFile(path.join(__dirname, 'client', 'build', 'index.html'));
});


app.listen(5004, () => {
    console.log("server is running at port 5004");
})